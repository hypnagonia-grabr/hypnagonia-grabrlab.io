### [Задание](https://gitlab.com/hypnagonia-grabr/hypnagonia-grabr.gitlab.io/blob/master/test-react.md)

### Установка зависимостей
````bash
 npm install
````

### Запуск в dev режиме
````bash
 npm start
````

Приложение будет доступно по
http://localhost:8080


