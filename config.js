module.exports = {
  'isDev': true,
  'host': {
    'frontendHost': 'localhost:80',
    'name': 'localhost',
    'ssl': false,
    'apiUrl': 'will be generated'
  },
  'dev': {
    '_': 'will be removed in production mode',
    'port': 8080
  }
}

