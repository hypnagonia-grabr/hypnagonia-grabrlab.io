import React from 'react'
import { Table, Button, Popconfirm, Modal } from 'antd'
import * as api from '../../../api/client'
import { displayDateFromNumbers } from '../../../utils'
import UserForm from './userFrom'

const columns = [
  {
    title: '#',
    dataIndex: 'id'
  },
  {
    title: 'ФИО',
    dataIndex: 'name'
  },
  {
    title: 'Дата рождения',
    dataIndex: 'birthDate',
    render: birthDate => displayDateFromNumbers(birthDate)
  },
  {
    title: 'Адрес',
    dataIndex: 'address'
  },
  {
    title: 'Город',
    dataIndex: 'city'
  },
  {
    title: 'Телефон',
    dataIndex: 'phone'
  }
]

export class UsersList extends React.Component {
  constructor () {
    super()
    this.state = {
      users: [],
      isUserFormModalVisible: false,
      selectedUser: {}
    }
  }

  toggleUserModal (selectedUser = {}) {
    this.setState({isUserFormModalVisible: !this.state.isUserFormModalVisible, selectedUser})
  }

  componentWillMount () {
    this.getUsers()
  }

  getUsers () {
    const users = api.getUsers()
    this.setState({users})
  }

  deleteUser (user) {
    api.deleteUser(user)
    this.getUsers()
  }

  saveUser (user) {
    api.saveUser(user)
    this.getUsers()
    this.toggleUserModal()
  }

  render () {
    const {users, selectedUser} = this.state

    const controls = {
      fixed: 'right',
      width: 70,
      render: (_, user) => {
        return (
          <div className='buttons' style={{display: 'flex', justifyContent: 'space-between', width: 70}}>
            <Button
              onClick={() => this.toggleUserModal(user)}
              type='primary'
              shape='circle'
              icon='edit'
              title='Редактировать'/>

            <Popconfirm
              title={'Удалить пользователя?'}
              onConfirm={() => this.deleteUser(user)}
              okText='Да'
              cancelText='Нет'>
              <Button
                type='danger'
                shape='circle'
                icon='close'
                title='Удалить'/>
            </Popconfirm>
          </div>
        )
      }
    }

    return (
      <div>
        {/* todo инлайновые стили для скорости написания */}
        <div style={{marginBottom: '14px'}}>
          <Button
            onClick={() => this.toggleUserModal()}
            type='primary'
            icon='plus'>
            Создать нового пользователя
          </Button>
        </div>
        <Table
          scroll={{x: true}}
          rowKey='id'
          columns={[...columns, controls]}
          dataSource={users}
          pagination={false}
        />

        {this.state.isUserFormModalVisible &&
        <Modal
          title='Пользователь'
          visible
          onCancel={() => this.toggleUserModal()}
          footer=''
        >
          <UserForm
            user={selectedUser}
            onSubmit={user => this.saveUser(user)}
          />
        </Modal>
        }
      </div>
    )
  }
}
