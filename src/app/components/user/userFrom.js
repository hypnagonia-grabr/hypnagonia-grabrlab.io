import React from 'react'
import { AutoComplete, Button, Form, Input, Select } from 'antd'
const FormItem = Form.Item
const Option = Select.Option

/**
 * я бы предпочел работать с обычным date picker инпутом
 * но следуя ТЗ "Дата рождения (обязательно, в форме трех селектов)"
 */
const daysList = [...Array(31).keys()].map(d => d + 1 + '')
const monthsList = [...Array(12).keys()].map(d => d + 1 + '')
const yearsList = [...Array(110).keys()].map(d => d + 1900 + '').reverse()

const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 8}
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 16}
  }
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0
    },
    sm: {
      span: 16,
      offset: 8
    }
  }
}

const newUser = {
  name: '',
  birthDate: {
    year: 2000,
    month: 12,
    day: 31
  },
  address: '',
  city: 'Москва',
  phone: '+79'
}

class UserForm extends React.Component {
  constructor () {
    super()
    this.state = {
      confirmDirty: false,
      autoCompleteResult: []
    }
    this.validateForm = this.validateForm.bind(this)
  }

  submitForm () {
    this.props.form.validateFieldsAndScroll(this.validateForm)
  }

  isNewUser () {
    return !(this.props.user && this.props.user.id !== undefined)
  }

  validateForm (err, values) {
    if (!err) {
      if (this.props.onSubmit) {
        values.phone = `+${values.prefix}${values.phone}`
        values.id = this.props.user.id
        this.props.onSubmit(values)
      }
      return
    }

    // не очень красивый хак, чтобы не разбираться с form Item декоратором для группировки вывода ошибок
    let birthDateErrors = []
    birthDateErrors = birthDateErrors.concat(((err.birthDate || {}).day || {}).errors || [])
    birthDateErrors = birthDateErrors.concat(((err.birthDate || {}).month || {}).errors || [])
    birthDateErrors = birthDateErrors.concat(((err.birthDate || {}).year || {}).errors || [])

    if (!birthDateErrors.length) {
      return
    }

    this.props.form.setFields({
      'birthDate.day': {
        errors: birthDateErrors
      }
    })
  }

  render () {
    const {getFieldDecorator} = this.props.form

    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '7'
    })(
      <Select style={{width: 50}}>
        <Option value='7'>+7</Option>
      </Select>
    )

    const user = this.isNewUser()
      ? newUser
      : this.props.user

    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem
          {...formItemLayout}
          label='ФИО'
        >
          {getFieldDecorator('name', {
            initialValue: user.name,
            rules: [{
              max: 100, message: 'ФИО должно быть не более 100 символов длиной!'
            }, {
              required: true, message: 'ФИО пользователя не может быть пустым'
            }]
          })(
            <Input />
          )}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label='Дата рождения'
        >
          <div style={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>

            {getFieldDecorator('birthDate.day', {
              initialValue: user.birthDate.day + '',
              rules: [
                {
                  required: true, message: 'День рождения должен быть указан',
                }
              ]
            })(
              <AutoComplete
                style={{width: '25%'}}
                dataSource={daysList}
                placeholder='День'
              />
            )}

            {getFieldDecorator('birthDate.month', {
              initialValue: user.birthDate.month + '',
              rules: [{
                required: true, message: 'Месяц рождения должен быть указан'
              }]
            })(
              <AutoComplete
                style={{width: '25%'}}
                dataSource={monthsList}
                placeholder='Месяц'
              />
            )}

            {getFieldDecorator('birthDate.year', {
              initialValue: user.birthDate.year + '',
              rules: [{
                required: true, message: 'Год рождения должен быть указан'
              }]
            })(
              <AutoComplete
                style={{width: '35%'}}
                dataSource={yearsList}
                placeholder='Год'
              />
            )}

          </div>

        </FormItem>

        <FormItem
          {...formItemLayout}
          label='Телефон'
        >
          {getFieldDecorator('phone', {
            initialValue: ((+user.phone + '').substring(1)),
            rules: [{
              max: 10,
              min: 10,
              message: 'Длина номера должна быть 11 знаков'
            }]
          })(
            <Input type='text' addonBefore={prefixSelector} style={{width: '100%'}}/>
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label='Адрес'
        >
          {getFieldDecorator('address', {
            initialValue: user.address
          })(
            <Input />
          )}
        </FormItem>

        <FormItem
          {...formItemLayout}
          label='Город'
        >
          {getFieldDecorator('city', {
            initialValue: user.city
          })(
            <Input />
          )}
        </FormItem>

        <FormItem {...tailFormItemLayout}>
          <Button type='primary' onClick={() => this.submitForm()}>
            {
              this.isNewUser() ? 'Создать' : 'Сохранить'
            }
          </Button>
        </FormItem>
      </Form>
    )
  }
}

export default Form.create()(UserForm)
