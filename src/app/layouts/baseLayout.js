import React from 'react'
import { Layout } from 'antd'
import { UsersList } from '../components/user/usersList'
const {Content, Footer} = Layout

export class BaseLayout extends React.Component {
  render () {
    return (
      <Layout style={{minHeight: '100vh'}}>
        <Content style={{margin: '16px'}}>
          <div style={{padding: 24, background: '#fff', minHeight: 360}}>
            <UsersList />
          </div>
        </Content>

        <Footer style={{textAlign: 'center'}}>
          USER CRUD ©2018
        </Footer>
      </Layout>
    )
  }
}
