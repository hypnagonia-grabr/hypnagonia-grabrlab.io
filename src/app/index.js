require('./styles/index.scss')
import React from 'react'
import ReactDOM from 'react-dom'
import 'antd/dist/antd.css'
import { LocaleProvider } from 'antd'
import ru from 'antd/lib/locale-provider/ru_RU'

import { App } from './app'

ReactDOM.render(
  <LocaleProvider locale={ru}>
    <App />
  </LocaleProvider>,
  document.getElementById('app')
)
