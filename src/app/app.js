import * as React from 'react'

import { BaseLayout } from './layouts'

export const App = () => {
  return (
    <BaseLayout />
  )
}
