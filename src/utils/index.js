/**
 * я бы предпочел работать с обычными date объектами
 * эта ситуация же родилась из ТЗ "Дата рождения (обязательно, в форме трех селектов)"
 */

export function displayDateFromNumbers (date) {
  let {day, month, year} = date

  day = +day
  month = +month
  year = +year

  if (day < 10) {
    day = '0' + day
  }

  if (month < 10) {
    month = '0' + month
  }
  return `${day}.${month}.${year}`
}
