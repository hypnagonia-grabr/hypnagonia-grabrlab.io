import { notification } from 'antd'

const usersMock = [
  {
    id: 1,
    name: 'Стародубцев Марат Адамович',
    birthDate: {
      year: 1980,
      month: 3,
      day: 20
    },
    address: 'ул. Льва Толстого, 16',
    city: 'Москва',
    phone: '+79856340123'
  },
  {
    id: 2,
    name: 'Распопов Велор Игнатьевич',
    birthDate: {
      year: 1982,
      month: 6,
      day: 10
    },
    address: 'Пискаревский проспект, дом 2, корпус 2',
    city: 'Санкт-Петербург',
    phone: '+78129222355'
  },
  {
    id: 3,
    name: 'Чюличкова Марфа Серафимовна',
    birthDate: {
      year: 1990,
      month: 11,
      day: 25
    },
    address: 'ул. Хохрякова, 10',
    city: 'Екатеринбург',
    phone: '+78228787778'
  }
]

export function getUsers () {
  let users
  try {
    users = JSON.parse(window.localStorage.getItem('users'))
  } catch (e) {
  }

  return users || usersMock
}

export function saveUser (user) {
  if (user.id === undefined) {
    createUser(user)
  } else {
    editUser(user)
  }
}

export function deleteUser (user) {
  const users = getUsers()
  const newUsers = users.filter(u => u.id !== user.id)
  saveUsers(newUsers)

  notification.success({
    message: 'Пользователь успешно удален'
  })
}

function createUser (user) {
  const users = getUsers()
  const nextId = users.reduce((p, u) => p > u.id ? p : u.id, 0) + 1
  user.id = nextId

  users.push(user)
  saveUsers(users)

  notification.success({
    message: 'Пользователь успешно создан'
  })
}

function editUser (user) {
  const users = getUsers()
  const index = users.findIndex(u => u.id === user.id)
  users[index] = user
  saveUsers(users)

  notification.success({
    message: 'Пользователь успешно отредактирован'
  })
}

function saveUsers (users) {
  window.localStorage.setItem('users', JSON.stringify(users))
}
